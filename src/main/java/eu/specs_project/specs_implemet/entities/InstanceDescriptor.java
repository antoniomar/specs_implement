package eu.specs_project.specs_implemet.entities;

public class InstanceDescriptor {
	private String image, hardwareId, location;

	public InstanceDescriptor(){
		
	}
	
	public InstanceDescriptor(String appliance, String zone, String hw) {
		this.image = appliance;
		this.hardwareId = hw;
		this.location = zone;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getHardwareId() {
		return hardwareId;
	}

	public void setHardwareId(String hardwareId) {
		this.hardwareId = hardwareId;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}
}
