package eu.specs_project.specs_implemet.entities.credentials;

public class ProviderCredential {
	private String username, password;
	
	public ProviderCredential(String username, String password) {
		super();
		this.username = username;
		this.password = password;
	}

	/**
	 * @return the user
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}
}
