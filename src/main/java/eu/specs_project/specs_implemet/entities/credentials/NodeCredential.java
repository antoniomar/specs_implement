package eu.specs_project.specs_implemet.entities.credentials;

public class NodeCredential {
	private String rootpassword, privatekey, publickey;
	
	public NodeCredential(String rootpassword,  String publickey, String privatekey) {
		super();
		this.rootpassword = rootpassword;
		this.privatekey= privatekey;
		this.publickey=publickey;
	}

	public String getRootpassword() {
		return rootpassword;
	}

	public String getPrivatekey() {
		return privatekey;
	}

	public String getPublickey() {
		return publickey;
	}
}
