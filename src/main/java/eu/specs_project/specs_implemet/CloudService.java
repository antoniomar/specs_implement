package eu.specs_project.specs_implemet;

import static com.google.common.base.Predicates.and;
import static com.google.common.base.Predicates.not;
import static com.google.common.collect.Iterables.getOnlyElement;
import static org.jclouds.compute.config.ComputeServiceProperties.TIMEOUT_SCRIPT_COMPLETE;
import static org.jclouds.compute.options.TemplateOptions.Builder.overrideLoginCredentials;
import static org.jclouds.compute.predicates.NodePredicates.TERMINATED;
import static org.jclouds.compute.predicates.NodePredicates.inGroup;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.jclouds.ContextBuilder;
import org.jclouds.compute.ComputeService;
import org.jclouds.compute.ComputeServiceContext;
import org.jclouds.compute.RunNodesException;
import org.jclouds.compute.domain.ExecResponse;
import org.jclouds.compute.domain.NodeMetadata;
import org.jclouds.compute.domain.Template;
import org.jclouds.compute.domain.TemplateBuilder;
import org.jclouds.compute.options.TemplateOptions;
import org.jclouds.domain.LoginCredentials;
import org.jclouds.scriptbuilder.ScriptBuilder;
import org.jclouds.scriptbuilder.domain.LiteralStatement;
import org.jclouds.scriptbuilder.domain.Statement;
import org.jclouds.scriptbuilder.statements.login.UserAdd;
import org.jclouds.scriptbuilder.statements.ssh.AuthorizeRSAPublicKeys;
import org.jclouds.scriptbuilder.statements.ssh.InstallRSAPrivateKey;
import org.jclouds.ssh.jsch.config.JschSshClientModule;

import com.google.common.collect.ImmutableSet;
import com.google.inject.Module;

import eu.specs_project.specs_implemet.ScriptExecutionResult.ScriptExecutionOutcome;
import eu.specs_project.specs_implemet.entities.InstanceDescriptor;
import eu.specs_project.specs_implemet.entities.NodesInfo;
import eu.specs_project.specs_implemet.entities.credentials.NodeCredential;
import eu.specs_project.specs_implemet.entities.credentials.ProviderCredential;



public class CloudService {
	private ComputeServiceContext context;
	private ComputeService compute;
	private String defaultUser;
	private Template template;
	
	public CloudService(String provider, String defaultUser, ProviderCredential providerCredential){
		Properties properties = new Properties();
	    long scriptTimeout = TimeUnit.MILLISECONDS.convert(20, TimeUnit.MINUTES);
	    properties.setProperty(TIMEOUT_SCRIPT_COMPLETE, scriptTimeout + "");
	    this.defaultUser = defaultUser;
	    
	    context = ContextBuilder.newBuilder(provider)
                .credentials(providerCredential.getUsername(), providerCredential.getPassword())
                .overrides(properties)
                .modules(ImmutableSet.<Module> of(
                		 new JschSshClientModule()))
                .buildView(ComputeServiceContext.class); 
	    compute = context.getComputeService();
	}
	
	private Template obtainTemplate(InstanceDescriptor descriptor, int... inboudports){
		TemplateBuilder templateBuilder = compute.templateBuilder();

		TemplateOptions options = new TemplateOptions();
		options.securityGroups("sla-no-restriction");

		template = templateBuilder
					.imageId(descriptor.getImage())
					.hardwareId(descriptor.getHardwareId())
					.locationId(descriptor.getLocation())
					//.options(inboundPorts(inboudports))
					.options(options)
					.build();	  
		return template;
	}
	
	public NodesInfo createNodesInGroup(String groupName, int numberOfInstances, InstanceDescriptor descriptor, NodeCredential nodeCredential, int... inboudports)  throws NoSuchElementException, Exception {	
		Set<? extends NodeMetadata> nodes = null;
		try {
			Template template = obtainTemplate(descriptor, inboudports);
			nodes = compute.createNodesInGroup(groupName, numberOfInstances, template);
		}catch(NoSuchElementException e){
			throw new NoSuchElementException(e.getMessage());
		}
		catch (Exception e) {
			destroyClusterWithName(groupName);
			e.printStackTrace();
			throw new Exception(e.getMessage());
		}
		NodesInfo info = new NodesInfo();
		for(NodeMetadata n : nodes){
			info.addNode(new ClusterNode(n.getId(), n.getPublicAddresses().iterator().next(), n.getPrivateAddresses().iterator().next()));
		}
		HashSet<String> pubKeys = new HashSet<String>();
		pubKeys.add(nodeCredential.getPublickey());
		AuthorizeRSAPublicKeys authKeys = new AuthorizeRSAPublicKeys(pubKeys);
		InstallRSAPrivateKey instPriv = new InstallRSAPrivateKey(nodeCredential.getPrivatekey());
		
		ScriptBuilder sb = new ScriptBuilder();

		sb.addStatement(authKeys);
		sb.addStatement(instPriv);
		ExecResponse er = null;
		for(NodeMetadata n : nodes){
			er = compute.runScriptOnNode(n.getId(), sb, 
				overrideLoginCredentials(getLoginForCommandExecution(defaultUser, n.getCredentials().credential.trim())).runAsRoot(false).wrapInInitScript(false));
			if(er.getExitStatus()!=0){
				destroyClusterWithName(groupName);
				throw new Exception("Error in configuration of custom RSA keys");
			}
		}
		
		LiteralStatement pass = new LiteralStatement(String.format("echo -e \"%s\n%s\n\" | passwd root", nodeCredential.getRootpassword(),nodeCredential.getRootpassword()));
		sb = new ScriptBuilder();
		sb.addStatement(pass);
		for(NodeMetadata n : nodes){
			er = compute.runScriptOnNode(n.getId(), sb, 
				overrideLoginCredentials(getLoginForCommandExecution(defaultUser, n.getCredentials().credential.trim())).runAsRoot(true).wrapInInitScript(false));
			if(er.getExitStatus()!=0){
				destroyClusterWithName(groupName);
				throw new Exception("Error in installing root password");
			}
		}
		info.setPublicKey(nodeCredential.getPublickey());
		info.setPrivateKey(nodeCredential.getPrivatekey());
		info.setRootPassword(nodeCredential.getRootpassword());
		return info;
	}
	
	
	public ClusterNode addNode(String groupName, String pubKey, String privKey, String rootPassword) throws Exception{
		NodeMetadata n = null;
		try{
			n = getOnlyElement(compute.createNodesInGroup(groupName, 1, template));
		} catch (RunNodesException e) {
			throw new Exception(e.getMessage());
		} finally{
			destroyClusterWithName(groupName);
		}
		HashSet<String> pubKeys = new HashSet<String>();
		pubKeys.add(pubKey);
		AuthorizeRSAPublicKeys authKeys = new AuthorizeRSAPublicKeys(pubKeys);
		InstallRSAPrivateKey instPriv = new InstallRSAPrivateKey(privKey);
		
		ScriptBuilder sb = new ScriptBuilder();

		sb.addStatement(authKeys);
		sb.addStatement(instPriv);
		ExecResponse er = compute.runScriptOnNode(n.getId(), sb, 
			overrideLoginCredentials(getLoginForCommandExecution(defaultUser, n.getCredentials().credential)).runAsRoot(false).wrapInInitScript(false));
		if(er.getExitStatus()!=0){
			destroyClusterWithName(groupName);
			throw new Exception("Error in configuration of custom RSA keys");
		}
		
		LiteralStatement pass = new LiteralStatement(String.format("echo \"%s\" | passwd --stdin root", rootPassword));
		sb = new ScriptBuilder();
		sb.addStatement(pass);
		er = compute.runScriptOnNode(n.getId(), sb, 
			overrideLoginCredentials(getLoginForCommandExecution(defaultUser, n.getCredentials().credential)).runAsRoot(true).wrapInInitScript(false));
		if(er.getExitStatus()!=0){
			destroyClusterWithName(groupName);
			throw new Exception("Error in installing root password");
		}
		
		return new ClusterNode(n.getId(), n.getPublicAddresses().iterator().next(), n.getPrivateAddresses().iterator().next());
	}
	
	
	
	
	public ScriptExecutionResult addNewUser(String userName, String password, String newPublicKey, String newPrivateKey, List<ClusterNode> nodes, String privateKeyDefaultUser){
		HashMap<String, String> info = new HashMap<String, String>();
		ScriptExecutionResult scriptResult = new ScriptExecutionResult();
		UserAdd.Builder userBuilder = UserAdd.builder();
        userBuilder.login(userName);
        userBuilder.authorizeRSAPublicKey(newPublicKey);
        userBuilder.installRSAPrivateKey(newPrivateKey);
        userBuilder.defaultHome("/home");
        if(password!=null){
        	userBuilder.password(password);
        }
        Statement userBuilderStatement = userBuilder.build();
        ExecResponse er = null;
       
        for(ClusterNode n : nodes){
        	er = compute.runScriptOnNode(n.getId(), userBuilderStatement,
    			overrideLoginCredentials(getLoginForCommandExecution(defaultUser, privateKeyDefaultUser)).runAsRoot(true).wrapInInitScript(false)); 
        	if(er.getExitStatus()!=0){
        		scriptResult.setOutcome(ScriptExecutionOutcome.GENERIC_ERROR);
				scriptResult.addAdditionalInfo("Error_Type", "New user configuration error");
				scriptResult.addNodeWithError(n);
			}
        }
        info.put("privateKeyNewUser", newPrivateKey);
        info.put("publicKeyNewUser", newPublicKey);
        info.put("username", userName);
        info.put("password", password);
        scriptResult.setAdditionalInfo(info);
        return scriptResult;
	}
	
	

		
	public ScriptExecutionResult executeScriptOnNode(String user, ClusterNode node, String[] instructions, String privateKey, boolean sudo){
		ScriptExecutionResult scriptResult = new ScriptExecutionResult();
		ScriptBuilder sb = new ScriptBuilder();
		for(String s : instructions){
			sb.addStatement(new LiteralStatement(s));
		}
		ExecResponse er = compute.runScriptOnNode(node.getId(), sb,
				overrideLoginCredentials(getLoginForCommandExecution(user, privateKey)).runAsRoot(sudo).wrapInInitScript(false));
		if(er.getExitStatus()!=0){
    		scriptResult.setOutcome(ScriptExecutionOutcome.GENERIC_ERROR);
			scriptResult.addAdditionalInfo("Error_Type", "New user configuration error");
			scriptResult.addNodeWithError(node);
		}
		scriptResult.addOutput(node.getId(), er.getOutput());
		return scriptResult;
	}
	
	
	public ScriptExecutionResult executeScriptOnNode(String user, ClusterNode node, Statement s, String privateKey, boolean sudo){
		ScriptExecutionResult scriptResult = new ScriptExecutionResult();
		ScriptBuilder sb = new ScriptBuilder();
		sb.addStatement(s);
		ExecResponse er = compute.runScriptOnNode(node.getId(), sb,
				overrideLoginCredentials(getLoginForCommandExecution(user, privateKey)).runAsRoot(sudo).wrapInInitScript(false));
		if(er.getExitStatus()!=0){
    		scriptResult.setOutcome(ScriptExecutionOutcome.GENERIC_ERROR);
			scriptResult.addAdditionalInfo("Error_Type", "New user configuration error");
			scriptResult.addNodeWithError(node);
		}
		scriptResult.addOutput(node.getId(), er.getOutput());
		return scriptResult;
	}
	
	public Map<String, ScriptExecutionResult> executeScriptOnNodes(String user, List<ClusterNode> nodes, String[] instructions, String privateKey, boolean sudo){
		Map<String, ScriptExecutionResult> map = new HashMap<String, ScriptExecutionResult>();
		ScriptExecutionResult temp;
		for(ClusterNode n : nodes){
			temp = executeScriptOnNode(user, n, instructions, privateKey, sudo);
			map.put(n.getId(), temp);
		}
		return map;
	}
	
	public Map<String, ScriptExecutionResult> executeScriptOnNodes(String user, List<ClusterNode> nodes,Statement s, String privateKey, boolean sudo){
		Map<String, ScriptExecutionResult> map = new HashMap<String, ScriptExecutionResult>();
		ScriptExecutionResult temp;
		for(ClusterNode n : nodes){
			temp = executeScriptOnNode(user, n, s, privateKey, sudo);
			map.put(n.getId(), temp);
		}
		return map;
	}

	
	//provare con rackspace start stop reboot
	public void suspendNodesInGroup(List<ClusterNode> hosts){
		for (int i = 0; i<hosts.size(); i++){
			compute.suspendNode(hosts.get(i).getId());
		}
	}
	
	public void resumeNodesInGroup(List<ClusterNode> hosts){
		for (int i = 0; i<hosts.size(); i++){
			compute.resumeNode(hosts.get(i).getId());
		}
	}

	public void rebootNodesInGroup(List<ClusterNode> hosts){
		for (int i = 0; i<hosts.size(); i++){
			compute.rebootNode(hosts.get(i).getId());
		}
	}
	
	//provare con versione 1.7
	public void destroyCluster(List<ClusterNode> hosts){ //distrugge anche keypair associati e il security group se non ci sono altre risorse pendenti associate
		for (int i = 0; i<hosts.size(); i++){
			compute.destroyNode(hosts.get(i).getId());
		}
	}
	
	public void destroyClusterWithName(String groupName){
		try{
			compute.destroyNodesMatching(and(inGroup(groupName), not(TERMINATED)));
		} catch(Exception e){
			e.getMessage();
		}
	}
	
	public void destroySingleNode(ClusterNode node){
		compute.destroyNode(node.getId());
	}
		
	private LoginCredentials getLoginForCommandExecution(String user, String credential) {
	
	    return LoginCredentials.builder().user(user).credential(credential).build();   
	}


		
	
public class ExecuteStatementsOnNode  extends Thread{
		
		public ExecuteStatementsOnNode(String user, ClusterNode node,
				Statement statement, String privateKey, boolean sudo,
				CloudService compute) {
			super();
			this.user = user;
			this.node = node;
			this.statement = statement;
			this.privateKey = privateKey;
			this.sudo = sudo;
			this.compute = compute;
		}

		private String user;
		private ClusterNode node;
		private Statement statement;
		private String privateKey;
		boolean sudo;
		private CloudService compute;
		
		public void run(){ 
		
			compute.executeScriptOnNode(user, node,statement,privateKey,sudo);
		
		}
		}
	
	public class ExecuteIstructionsOnNode  extends Thread{
		
		private String user;
		private ClusterNode node;
		private String[] istructions;
		private String privateKey;
		boolean sudo;
		private CloudService compute;
		
		public ExecuteIstructionsOnNode(String user, ClusterNode node,
				String[] istructions, String privateKey, boolean sudo,
				CloudService compute) {
			super();
			this.user = user;
			this.node = node;
			this.istructions = istructions;
			this.privateKey = privateKey;
			this.sudo = sudo;
			this.compute = compute;
		}
		
		public void run(){ 
		
			compute.executeScriptOnNode(user, node,istructions,privateKey,sudo);
		
		}
	}
	
	
	
	//TODO: try to split this class 
	
	
	
	
}
