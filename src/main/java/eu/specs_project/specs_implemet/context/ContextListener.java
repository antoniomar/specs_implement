package eu.specs_project.specs_implemet.context;

import java.util.Properties;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

@WebListener
public class ContextListener implements ServletContextListener {

	private final String confFile ="confFile";
	private final String resourceFolder ="resources/";
	
	
	@Override
	public void contextDestroyed(ServletContextEvent arg0) {
		System.out.println("ServletContextListener destroyed");
	}
 
    //Run this before web application is started
	@Override
	public void contextInitialized(ServletContextEvent arg0) {
		System.out.println("ServletContextListener started");	
		
		try{
			Properties prop = new Properties();
			String initParam = arg0.getServletContext().getInitParameter(confFile);
			prop.load(Thread.currentThread().getContextClassLoader().getResourceAsStream(resourceFolder+initParam));
			
			//String test2 = prop.getProperty("implement.templates.dir");
			//arg0.getServletContext().setAttribute("slaTemplate", slatemplate.toString());
			
		}catch (Exception e){
			e.printStackTrace();
		}
		
		
	}
	
}
